<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 12:58 AM
 */

namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class City extends DB
{
    private $id;
    private $name;
    private $city;
    private $postCode;
    private $postOffice;
    private $policeStation;
    private $detailAddress;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("city",$allPostData)){
            $this -> city = $allPostData['city'];
        }
        if(array_key_exists("post_code",$allPostData)){
            $this-> postCode = $allPostData['post_code'];
        }
        if(array_key_exists("post_office",$allPostData)){
            $this-> postOffice = $allPostData['post_office'];
        }
        if(array_key_exists("police_station",$allPostData)){
            $this-> policeStation = $allPostData['police_station'];
        }
        if(array_key_exists("detail_address",$allPostData)){
            $this-> detailAddress = $allPostData['detail_address'];
        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->city, $this-> postCode, $this-> postOffice, $this-> policeStation, $this->detailAddress);
        $query = 'INSERT INTO city (user_name, 	city, post_code, post_office, police_station, detail_address) VALUES (?,?,?,?,?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('city.php');
    }

}