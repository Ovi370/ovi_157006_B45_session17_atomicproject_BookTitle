<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 1:16 AM
 */

namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Email extends DB
{
    private $id;
    private $name;
    private $email;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this-> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this-> name = $allPostData['user_name'];
        }
        if(array_key_exists("email",$allPostData)){
            $this-> email = $allPostData['email'];
        }
    }

    public function store(){
        $arrayData = array($this-> name,$this->email);
        $query = 'INSERT INTO email (user_name, email) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('email.php');
    }

}