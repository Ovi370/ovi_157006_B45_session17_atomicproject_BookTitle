<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 1:39 AM
 */

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Hobbies extends DB
{
    private $id;
    private $name;
    private $hobbies;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("hobbies",$allPostData)){
            $this -> hobbies = $allPostData['hobbies'];
        }
    }

    public function store(){
        $arrayData = array($this-> name,$this->hobbies);
        $query = 'INSERT INTO hobbies (user_name, hobbies) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('hobbies.php');
    }

}