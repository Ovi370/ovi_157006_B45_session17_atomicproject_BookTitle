<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 2:31 PM
 */

namespace App\Model;

use PDO, PDOException;

class Database
{

    public  $DBH;

    public function __construct()
    {

        try{
            $this->DBH = new PDO('mysql:host=localhost;dbname=atomic_project_database', "root", "");
            $this-> DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Database Connection Successful! <br>";
        }
        catch(PDOException $error){
            echo "Database Error". $error-> getMessage()."<br>";
        }



    }


}