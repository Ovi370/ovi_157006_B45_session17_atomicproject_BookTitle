<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 2:00 AM
 */

namespace App\Profile_Picture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $filename;
    private $tempLocation;
    private $success;
    private $error;
    private $dir;
    private $src;

    public function setDir()
    {
        $this->dir = "images/";
    }

    public function setSrc($src)
    {
        $this->src = $this->dir.time().$this->filename;
    }

    public function getSrc()
    {
        return $this->src;
    }

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this-> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this-> name = $allPostData['user_name'];
        }

    }
    public function setProPic($allFileData=null){
        if(array_key_exists("file",$allFileData)){



            $this-> filename = $allFileData['file']['name'];
            $this-> tempLocation= $allFileData['file']['tmp_name'];
            $this-> src = $this->dir.time().$this->filename;
            $this-> success = move_uploaded_file($this -> tempLocation,$this->src);
            $this-> error= $allFileData['file']['error'];
            if($this-> success || $this-> error)
            {
                echo "successfully moved";
                echo "<hr>";
            }
            else
            {
                echo "An error has occured";
                echo "<hr>";
            }

        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->src);
        $query = 'INSERT INTO profile_picture (user_name, profile_picture) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('profile_picture.php');
    }

}