<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 1:51 AM
 */

namespace App\Summary_Of_Organization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Summary_Of_Organization extends DB
{
    private $id;
    private $name;
    private $summary_of_organization;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("summary_of_organization",$allPostData)){
            $this -> summary_of_organization = $allPostData['summary_of_organization'];
        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->summary_of_organization);
        $query = 'INSERT INTO summary_of_organization (user_name, summary_of_organization) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('summary_of_organization.php');
    }

}